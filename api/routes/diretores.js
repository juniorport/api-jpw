const express = require('express')
const router = express.Router()
const Diretores = require('../models/diretores')
const auth = require('../middlewares')


//Cadatra um Diretores
router.post("/novo",auth.auth, (req, res) => {
    const nome = req.body.nome
    if (nome === undefined) {
        res.status(400).send("Informe o nome do Diretor")
    } else {
        const novoUsuario = req.body
        new Diretores(novoUsuario).save().then(() => {
            res.status(200).send("Usuario salvo com Sucesso")
        }).catch((err) => {
            res.status(400).send("Erro ao salvar o Diretor: " + err)
        })
    }
})


// Lista todos os Diretores
router.get('/', async (req, res) => {
    try {
        var users = await Diretores.find()
        return res.status(200).send({ users })
    } catch (erro) {
        return res.status(400).send({ erro: "Erro ao carregar os Diretores" })
    }
})


//Lista apenas um Diretor por id
router.get('/:id', async (req, res) => {
    try {
        await Diretores.findOne({ nome: req.params.id }, (err, doc) => {
            if (doc == null) return res.status(400).send("Dados incorretos")
            else return res.status(200).send({ doc })
        })
    } catch (erro) {
        return res.status(400).send({ erro: "Erro ao carregar os Diretor Catch" })
    }
})
 

//Atualiza um Diretor 
router.put('/:id',auth.auth, async (req, res) => {
    try {
        const filtro = req.params.id
        const update = req.body
        if (update === undefined) {
            res.status(400).send("Informe o nome do Diretor")
        } else {
            await Diretores.findOneAndUpdate({ nome: filtro }, update, (err, doc) => {
                if (doc == null) {
                    return res.status(400).send("Diretor não esta cadastrado verifique o nome informado")
                } else {
                    res.status(200).send({ doc })
                }
            })
        }
    }
    catch (erro) {
        return res.status(400).send({ erro: "Erro ao carregar os Diretor" })
    }
})


//Deletar Um Diretor
router.delete('/:id',auth.auth, async (req, res) => {
    try {
        await Diretores.findOneAndDelete({ nome: req.params.id }, (err, doc) => {
            if (err) {
                return err
            }
            else {
                if (doc == null) {
                    return res.status(400).send("Diretor não Existe")
                }
                else {
                    return res.status(200).send({ doc })
                }
            }
        })
    } catch (erro) {
        return res.status(400).send({ erro: "Erro ao carregar os Diretor" })
    }
})


 //Filtra com limite
 router.get('/listar/diretores', async (req, res)=>{
    const limite = req.query.limit
    const n = Number(limite)  
     try {
         var users = await Diretores.find().limit(n)
         res.send({users})
 
     } catch (erro) {
         return res.status(400).send({ erro: "Erro ao carregar os Diretores" })
     }     
})
   
  
 //Filtra 
 router.get('/listar/filter', async(req, res)=>{
     const filtro = req.query
     console.log(filtro)
      try {
          var users = await Diretores.find(filtro)
          res.send({users})
  
      } catch (erro) {
          return res.status(400).send({ erro: "Erro ao carregar os Diretores" })
      }
      
})


module.exports = router

//Testado todas as funçoes