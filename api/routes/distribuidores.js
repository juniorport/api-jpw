const express = require('express')
const router = express.Router()
const Distribuidores = require('../models/distribuidores')
const auth = require('../middlewares')


//Cadatra um Distribuidor
router.post("/novo",auth.auth, (req, res) => {
    const nome = req.body.nome
    if (nome === undefined) {
        res.status(400).send("Informe o nome do Distribuidor")
    } else {
    const novoCadastro = req.body
    new Distribuidores(novoCadastro).save().then(() => {
        res.status(200).send({ novoUsuario: novoCadastro })
    }).catch((err) => {
        res.status(400).send("Erro ao salvar o Distribuidor" + err)
    })
    }
})
 

// Lista todos os Distribuidores
router.get('/', async (req, res) => {
    try {
        var distribuidor = await Distribuidores.find()
        return res.status(200).send({ users: distribuidor })
    } catch (erro) {
        return res.status(400).send({ erro: "Erro ao carregar os Distribuidor" })
    }
})


//Lista apenas um Distribuidor por id
router.get('/:id', async (req, res) => {
    try {
        await Distribuidores.findOne({ nome: req.params.id }, (err, doc) => {
            if (doc == null) return res.status(400).send("Verifique o Distribuidor informado")
            else return res.status(200).send({ doc })
        })
    } catch (erro) {
        return res.status(400).send({ erro: "Erro ao carregar os Distribuidor" })
    }
})


//Atualiza um Distribuidor 
router.put('/:id',auth.auth, async (req, res) => {
    try {
        const filtro = req.params.id
        const update = req.body
        if (update === undefined) {
            res.status(400).send("Informe o dado que deseja atualizar no Distribuidor")
        } else {
        await Distribuidores.findOneAndUpdate({ nome: filtro}, update, (err, doc) => {
            if (doc == null) {
                return res.status(400).send("Usuario não esta cadastrado")
            } else {
                res.status(200).send({ doc })
            }
        })
    }
    }
    catch (erro) {
        return res.status(400).send({ erro: "Erro ao carregar os Distribuidor " })
    }
})


//Deletar Um Distribuidor
router.delete('/:id',auth.auth, async (req, res) => {
    try {
        await Distribuidores.findOneAndDelete({ nome: req.params.id }, (err, doc) => {
            if (err) {
                return res.status(400).send(err)
            }
            else {
                if (doc == null) {
                    return res.status(400).send("Distribuidor não Existe")
                }
                else {
                    return res.status(200).send({ doc })
                }
            }
        })
    } catch (erro) {
        return res.status(400).send({ erro: "Erro ao carregar os Distribuidor" })
    }
})


//Lista por Limite
router.get('/listar/distribuidores', async (req, res)=>{

    const limite = req.query.limit
    const n = Number(limite)
    
     try {
         var users = await Distribuidores.find().limit(n)
         res.status(200).send({users})
 
     } catch (erro) {
         return res.status(400).send({ erro: "Erro ao carregar os Distribuidores" })
     }
     
})


//Lista por Filtro
 router.get('/listar/distribuidores', async(req, res)=>{
     const filtro = req.query
     console.log(filtro)
      try {
          var users = await Distribuidores.find(filtro)
          res.status(200).send({users})
  
      } catch (erro) {
          return res.status(400).send({ erro: "Erro ao carregar os Distribuidores" })
      }   
  }) 


module.exports = router