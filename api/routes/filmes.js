const express = require('express')
const router = express.Router()
const Filmes = require('../models/filmes')
const auth = require('../middlewares')


//Cadatra um Filme
router.post("/novo",auth.auth, (req, res) => {
    const nome = req.body.nome
    if (nome === undefined) {
        res.status(400).send("Informe o nome do Diretor")
    } else {
    const novoCadastro = req.body
    new Filmes(novoCadastro).save().then(() => {
        res.status(200).send({ novoUsuario: novoCadastro })
    }).catch((err) => {
        res.status(400).send("Erro ao salvar o Filme" + err)
    })
    }
})


// Lista todos os Filmes
router.get('/', async (req, res) => {
    try {
        var filmes = await Filmes.find().populate('genero').populate('distribuidor').populate('diretor')
        return res.status(200).send({ users: filmes })
    } catch (erro) {
        return res.status(400).send({ erro: "Erro ao carregar os Filme" })
    }
})


//Lista apenas um Filme por id
router.get('/:id', async (req, res) => {
    try {
        await Filmes.findOne({ nome: req.params.id }, (err, doc) => {
            if (doc == null) return res.status(400).send("Dados incorretos")
            else return res.status(200).send({ doc })
        })
    } catch (erro) {
        return res.status(400).send({ erro: "Erro ao carregar os Filme Catch" })
    }
})


//Atualiza um Filme 
router.put('/:id',auth.auth, async (req, res) => {
    try {
        const filtro = req.params.id
        const update = req.body
        console.log(update)
        if (update === undefined ) {
            res.status(400).send("Informe o dado que deseja atualizar no Filme")
        } else {

        await Filmes.findOneAndUpdate({nome: filtro}, update, (err, doc) => {
                if (doc == null){
                 return res.status(400).send("Usuario não esta cadastrado")
                }else{
                 res.status(200).send({doc})
                }                 
        })
    }
    } 
    catch (erro) {
        return res.status(400).send({ erro: "Erro ao carregar os Filme Catch" })
    }
})


//Deletar Um Filme
router.delete('/:id',auth.auth, async (req, res) => {
    try {
        await Filmes.findOneAndDelete({ nome: req.params.id }, (err, doc) => {
            if (err) {
                return err
            } 
            else {
                if (doc == null) {
                    return res.status(400).send("Filme não Existe")
                }
                 else {
                    return res.status(200).send({ doc })
                }
            }
        })
    } catch (erro) {
        return res.status(400).send({ erro: "Erro ao carregar os Filme " })
    }
})


//Lista por Limite
router.get('/listar/filmes', async (req, res)=>{

    const limite = req.query.limit
    const n = Number(limite)
    
     try {
         var users = await Filmes.find().limit(n)
         res.status(200).send({users})
 
     } catch (erro) {
         return res.status(400).send({ erro: "Erro ao carregar os Filmes" })
     }
     
})


//Lista por Filtro
router.get('/listar/filmes', async(req, res)=>{
     const filtro = req.query
     console.log(filtro)
      try {
          var users = await Filmes.find(filtro).populate()
          res.status(200).send({users})
  
      } catch (erro) {
          return res.status(400).send({ erro: "Erro ao carregar os Filmes" })
      }
      
}) 


module.exports = router