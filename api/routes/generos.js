const express = require('express')
const router = express.Router()
const Generos = require('../models/generos') 
const auth = require('../middlewares')



//Cadatra um Genero
router.post("/novo",auth.auth, (req, res) => {
    const nome = req.body.nome
    if (nome === undefined) {
        res.status(400).send("Informe o nome do Genero")
    } else {
    const novoCadastro = req.body
    Generos.findOne({nome: nome}, (err, doc)=>{
        if(doc == null){
            new Generos(novoCadastro).save().then(() => {
                res.status(200).send({ novoUsuario: novoCadastro })
            }).catch((err) => {
                res.status(400).send("Erro ao salvar o Genero" + nome)
            })
        }else{
            res.status(400).send("O genero *"+nome+"* já esta cadastrado")
        }      
    })
    }
})


// Lista todos os Generos
router.get('/', async (req, res) => {
    try {
        var users = await Generos.find()
        return res.status(200).send({ users })
    } catch (erro) {
        return res.status(400).send({ erro: "Erro ao carregar os Generos" })
    }
})


//Lista apenas um Genero por id
router.get('/:id', async (req, res) => {
    try {
        await Generos.findOne({ nome: req.params.id }, (err, doc) => {
            if (doc == null) return res.status(400).send("Verifique o nome Informado")
            else return res.status(200).send({ doc })
        })
    } catch (erro) {
        return res.status(400).send({ erro: "Erro ao carregar os Genero " })
    }
})


//Atualiza um Genero 
router.put('/:id',auth.auth, async (req, res) => {
    try {
        const filtro = req.params.id
        const update = req.body
        await Generos.findOneAndUpdate({nome: filtro}, update, (err, doc) => {
                if (doc == null){
                 return res.status(400).send("Erro ao genero não esta cadastrado")
                }else{
                 res.status(200).send({doc})
                }                 
        })
    } 
    catch (erro) {
        return res.status(400).send({ erro: "Erro ao carregar os Genero" })
    }
})


//Deletar Um Genero
router.delete('/:id',auth.auth, async (req, res) => {
    try {
        await Generos.findOneAndDelete({ nome: req.params.id }, (err, doc) => {
            if (err) {
                return err
            } 
            else {
                if (doc == null) {
                    return res.status(400).send("Genero não Existe")
                }
                 else {
                    return res.status(200).send({ doc })
                }
            }
        })
    } catch (erro) {
        return res.status(400).send({ erro: "Erro ao carregar os Genero" })
    }
})


//Lista por limite
router.get('/listar/generos', async (req, res)=>{

    const limite = req.query.limit
    const n = Number(limite)
    
     try {
         var users = await Generos.find().limit(n)
         res.send({users})
 
     } catch (erro) {
         return res.status(400).send({ erro: "Erro ao carregar os Generos" })
     }
     
})
 

//Lista por filtro
 router.get('/listar/generos', async(req, res)=>{
     const filtro = req.query
     console.log(filtro)
      try {
          var users = await Generos.find(filtro)
          res.send({users})
  
      } catch (erro) {
          return res.status(400).send({ erro: "Erro ao carregar os Generos" })
      }
      
}) 


  module.exports= router
//Testado todas as funçoes