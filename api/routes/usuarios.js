const express = require('express')
const router = express.Router()
const Usuarios = require('../models/usuarios')
const jwt = require('jsonwebtoken')
const config = require('../config/autorizacao.json')
const auth = require('../middlewares')


//Cadatra um ususario
router.post("/novo",auth.auth, (req, res) => {
    const nome = req.body.email
    console.log(nome)
    if (nome === undefined) {
        res.status(400).send("Informe o nome do Diretor")
    } else {
    const novoUsuario = req.body
    new Usuarios(novoUsuario).save().then(() => {
        res.status(200).send({ novoUsuario })
    }).catch((err) => {
        res.status(400).send("Erro ao salvar o Usuario" + err)
    })
    }
})


// Lista todos os Usuarios
router.get('/', auth.auth, async (req, res) => {
    try {
        var users = await Usuarios.find()
        return res.status(200).send({ users })
    } catch (erro) {
        return res.status(400).send({ erro: "Erro ao carregar os Usuarios" })
    }
})


//Lista apenas um usuario por id
router.get('/:id', async (req, res) => {
    try {
        await Usuarios.findOne({ email: req.params.id }, (err, doc) => {
            if (doc == null) return res.status(400).send("Dados incorretos")
            else return res.status(200).send({ doc })
        })
    } catch (erro) {
        return res.status(400).send({ erro: "Erro ao carregar os Usuario Catch" })
    }
})


//Atualiza um usuario 
router.put('/:id', auth.auth, async (req, res) => {
    try {
        const filtro = req.params.id 
        const update = req.body
        await Usuarios.findOneAndUpdate({email: filtro}, update, (err, doc) => {
                if (doc == null){
                 return res.status(400).send("Usuario não esta cadastrado")
                }else{
                 res.status(200).send({doc})
                }                 
        })
    } 
    catch (erro) {
        return res.status(400).send({ erro: "Erro ao carregar os Usuario Catch" })
    }
})


//Deletar Um Usuario
router.delete('/:id', auth.auth, async (req, res) => {
    try {
        await Usuarios.findOneAndDelete({ email: req.params.id }, (err, doc) => {
            if (err) {
                return err
            } 
            else {
                if (doc == null) {
                    return res.status(400).send("Usuario não Existe")
                }
                 else {
                    return res.status(200).send({ doc })
                }
            }
        })
    } catch (erro) {
        return res.status(400).send({ erro: "Erro ao carregar os Usuario Catch" })
    }
})


//Filtra com limite
router.get('/listar/usuarios', async (req, res)=>{

   const limite = req.query.limit
   const n = Number(limite)
   
    try {
        var users = await Usuarios.find().limit(n)
        res.send({users})

    } catch (erro) {
        return res.status(400).send({ erro: "Erro ao carregar os Usuarios" })
    }
    
})

//Filtra 
router.get('/listar/filter', async(req, res)=>{
    const filtro = req.query
    console.log(filtro)
     try {
         var users = await Usuarios.find(filtro)
         res.send({users})
 
     } catch (erro) {
         return res.status(400).send({ erro: "Erro ao carregar os Usuarios" })
     }
     
}) 
 

//Autenticação de Login
router.post("/autentica", async (req, res) => {
    try {
    const email = req.body.email
    const senha = req.body.senha
    await Usuarios.findOne({ email: email},(err,doc)=>{
      if(err){
          return res.status(500).send({error: 'Banco não conectado'})
      }
      else{
          if(doc === null){
            return res.status(200).send("Verifique o Usuario")
          }
          if(senha === doc.senha){
              const token= jwt.sign({id: doc.email}, config.secret,{
                 expiresIn: 86400, 
                })
              return res.header('auth',token).status(200).send({doc,token})
          }
          else{
              return res.status(400).send("Senha Incorreta")
          }
      }
    })
    } catch (err) {
        res.status(400).send("Verifique a senha ou email informado")
    }
})


module.exports = router
