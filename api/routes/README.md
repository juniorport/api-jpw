# ROUTES <h2>
 Nesta pasta contem todas as rotas que serão utilizadas por nossa aplicação vamos listar em duas Tabelas  
 **Tabela de Rotas** - Será listada todas as rotas de nossa API  
 **Tabela de Testes** - Será listado todos os testes feitos em nossa api pelo Insominia
 **{{ base_url  }}** - É a URL base que você esta utilizando depende da porta que a API esta rodando exemplo "http://localhost:8080"  
 
 # Tabela de Rotas Diretores <h1>  
  
                                            
DIRETORES |                    BUSCA |                                                      TESTE |                                                                  ENVIO |                                         RETORNO |     
:--------- | :------:| :------:| :------:| :------:  
ADICIONAR         |POST:{{base_url}}/diretores/novo                          | POST:{{base_url}}/diretores/novo                                    | {"nome":"Christopher ","cidade": "Londresaaa"}  |"Usuario salvo com Sucesso"
LISTAR TODOS      |GET:{{ base_url  }}/diretores                            | GET:{{ base_url  }}/diretores                                      |              ---                                |Lista todos os Diretores do Banco
LISTAR APENAS UM  |GET:{{ base_url  }}/diretores/:id                        |GET:{{ base_url  }}/diretores/Christopher Nolan                     |              ---                                |Lista o Diretor pelo nome Informado no GET
ATUALIZAR         |PUT:{{ base_url  }}/diretores/:id                        |PUT:{{ base_url  }}/diretores/Deu certo                             |    { "nome": "Steven"}                          |Retorna o nome Atualizado
DELETAR           |DELETE:{{ base_url  }}/diretores/:id                     |DELETE:{{ base_url  }}/diretores/David Yates                        |              ---                                |Retorna o nome Deletado
FILTRO COM LIMITE |GET: {{ base_url  }}/diretores/listar/diretores?limit:id | GET: {{ base_url  }}/diretores/listar/diretores?limit:2            |              ---                                |Lista de acordo com o limite informado
FILTRAR           |GET {{ base_url  }}/diretores/listar/filter?filtro:id    | GET {{ base_url  }}/diretores/listar/?cidade= Westminster, Londres |              ---                                |Lista de acordo com o Filtro informado



# Tabela de Rotas Distribuidores <h1>  
  
                                            
DISTRIBUIDORES |                    BUSCA |                                                      TESTE |                                                                  ENVIO |                                         RETORNO |     
:--------- | :------:| :------:| :------:| :------:  
ADICIONAR         |POST:{{base_url}}/distribuidores/novo                          | POST:{{base_url}}/distribuidores/novo                                    | {"nome":"Americine do Brasil","cidade": "Rio de Janeiro"}  |Usuario cadastrado
LISTAR TODOS      |GET:{{ base_url  }}/distribuidores                            | GET:{{ base_url  }}/distribuidores                                      |              ---                                            |Lista todos os Distribuidores do Banco
LISTAR APENAS UM  |GET:{{ base_url  }}/distribuidores/:id                        |GET:{{ base_url  }}/distribuidores/Americine do Brasil                   |              ---                                            |Lista o Distribuidor pelo nome Informado no GET
ATUALIZAR         |PUT:{{ base_url  }}/distribuidores/:id                        |PUT:{{ base_url  }}/distribuidores/20th Century Fox                      |    { "cidade": "São Jõao"}                                  |Retorna o nome que foi atualizado
DELETAR           |DELETE:{{ base_url  }}/distribuidores/:id                     |DELETE:{{ base_url  }}/distribuidores/RFilms                             |              ---                                            |Retorna o nome Deletado
FILTRO COM LIMITE |GET: {{ base_url  }}/distribuidores/listar/diretores?limit:id | GET: {{ base_url  }}/distribuidores/listar/diretores?limit:2            |              ---                                            |Lista de acordo com o limite informado
FILTRAR           |GET {{ base_url  }}/distribuidores/listar/filter?filtro:id    | GET {{ base_url  }}/distribuidores/listar/?cidade= Canela               |              ---                                            |Lista de acordo com o Filtro informado
 
 
 # Tabela de Rotas Filmes<h1>
  
                                            
FILMES   |             BUSCA |                                                      TESTE |                                                                  ENVIO |                                         RETORNO |     
:--------- | :------:| :------:| :------:| :------:  
ADICIONAR         |POST:{{base_url}}/filmes/novo                          | POST:{{base_url}}/filmes/novo                                    | {"nome":"Mulher gato ","sinopse": "Mulher do Batman"}  |Usuario cadastrado
LISTAR TODOS      |GET:{{ base_url  }}/filmes                            | GET:{{ base_url  }}/filmes                                      |              ---                                       |Lista todos os Filmes do Banco
LISTAR APENAS UM  |GET:{{ base_url  }}/filmes/:id                        |GET:{{ base_url  }}/filmes/Mulher gato                           |              ---                                       |Lista o Filme pelo nome Informado no GET
ATUALIZAR         |PUT:{{ base_url  }}/filmes/:id                        |PUT:{{ base_url  }}/filmes/Mulher gato                           |    { "sinopse": "Sinopse atualizada"}                  |Retorna o nome Atualizado
DELETAR           |DELETE:{{ base_url  }}/filmes/:id                     |DELETE:{{ base_url  }}/filmes/Mulher gato                        |              ---                                       |Retorna o nome Deletado
FILTRO COM LIMITE |GET: {{ base_url  }}/filmes/listar/diretores?limit:id | GET: {{ base_url  }}/filmes/listar/diretores?limit:2            |              ---                                       |Lista de acordo com o limite informado
FILTRAR           |GET {{ base_url  }}/filmes/listar/filter?filtro:id    | GET {{ base_url  }}/filmes/listar/?nome=Matrix                  |              ---                                       |Lista de acordo com o Filtro informado

# Tabela de Rotas Generos<h1>
  
                                            
GENEROS   |            BUSCA |                                                      TESTE |                                                                  ENVIO |                                         RETORNO |     
:--------- | :------:| :------:| :------:| :------:  
ADICIONAR         |POST:{{base_url}}/gens/novo                          | POST:{{base_url}}/gens/novo                                    | {"nome":"Ficção Cientifica"}                    |Usuario cadastrado
LISTAR TODOS      |GET:{{ base_url  }}/gens                            | GET:{{ base_url  }}/gens                                      |              ---                                |Lista todos os Genero do Banco
LISTAR APENAS UM  |GET:{{ base_url  }}/gens/:id                        |GET:{{ base_url  }}/gens/Ficção Cientifica                     |              ---                                |Lista o Genero pelo nome Informado no GET
ATUALIZAR         |PUT:{{ base_url  }}/gens/:id                        |PUT:{{ base_url  }}/gens/Ficção Cientifica                     |    { "nome": "Ficção Cientifica atualizada"}    |Retorna o nome Atualizado
DELETAR           |DELETE:{{ base_url  }}/gens/:id                     |DELETE:{{ base_url  }}/gens/Ficção Cientifica atualizada       |              ---                                |Retorna o nome Deletado
FILTRO COM LIMITE |GET: {{ base_url  }}/gens/listar/diretores?limit:id | GET: {{ base_url  }}/gens/listar/diretores?limit:2            |              ---                                |Lista de acordo com o limite informado
FILTRAR           |GET {{ base_url  }}/gens/listar/filter?filtro:id    | GET {{ base_url  }}/gens/listar/?nome= Acao                   |              ---                                |Lista de acordo com o Filtro informado

# Tabela de Rotas Usuarios<h1>
  
                                            
USUARIOS     |                 BUSCA |                                                      TESTE |                                                                  ENVIO |                                         RETORNO |     
:--------- | :------:| :------:| :------:| :------:  
ADICIONAR         |POST:{{base_url}}/users/novo                          | POST:{{base_url}}/users/novo                                    | {"nome":"Carlos ","email": "carlos","senha": "admin"}  |"Usuario salvo com Sucesso"
LISTAR TODOS      |GET:{{ base_url  }}/users                            | GET:{{ base_url  }}/users                                      |              ---                                       |Lista todos os Diretores do Banco
LISTAR APENAS UM  |GET:{{ base_url  }}/users/:id                        |GET:{{ base_url  }}/users/Carlos                                |              ---                                       |Lista o Diretor pelo nome Informado no GET
ATUALIZAR         |PUT:{{ base_url  }}/users/:id                        |PUT:{{ base_url  }}/users/Carlos                                |    { "nome": "Carlos Rocha"}                           |Retorna o nome Atualizado
DELETAR           |DELETE:{{ base_url  }}/users/:id                     |DELETE:{{ base_url  }}/users/dassa                              |              ---                                       |Retorna o nome Deletado
FILTRO COM LIMITE |GET:{{ base_url  }}/users/listar/diretores?limit:id  | GET: {{ base_url  }}/users/listar/users?limit:2                |              ---                                       |Lista de acordo com o limite informado
FILTRAR           |GET:{{ base_url  }}/users/listar/filter?filtro:id    | GET {{ base_url  }}/users/listar/?admin= true                  |              ---                                       |Lista de acordo com o Filtro informado
AUTENTICAR        |POST: {{ base_url  }}/users/autentica                | {{ base_url  }}/users/autentica                                |   {"email": "Alterado de O segundo","senha": "admin"}  |Retorna o Token de Autenticação valido


# Dados do banco<h2>  

# Diretores<h1>   


{
      "_id": "5ec600938887ff2314622571",  
      "nome": "Steven Spielberg",  
      "cidade": "Cincinnati Cidade em Ohio ,  
      "nasc": "1970-01-01T04:26:51.992Z",  
      "criadoem": "2020-05-21T04:13:55.414Z",  
      "__v": 0  
    },  
    {  
      "_id": "5ec600958887ff2314622572",  
      "nome": "Peter Jackson\t",  
      "cidade": "Pukerua Bay, Porirua",  
      "nasc": "1970-01-01T04:26:51.992Z",  
      "criadoem": "2020-05-21T04:13:55.414Z",  
      "__v": 0  
    },  
    {
      "_id": "5ec600988887ff2314622573",  
      "nome": "Michael Bay",  
      "cidade": " Los Angeles",  
      "nasc": "1970-01-01T04:26:51.992Z",  
      "criadoem": "2020-05-21T04:13:55.414Z",  
      "__v": 0  
    },  
    {  
      "_id": "5ec6a3e7e3354c1cf0f3d1d9",  
      "nome": "James cameron",  
      "cidade": " Kapuskasing, Canadá",  
      "nasc": "1970-01-01T04:26:51.992Z",  
      "criadoem": "2020-05-21T04:13:55.414Z",  
      "__v": 0  
    },  
    { 
      "_id": "5ec928af7487b1167844a31a",  
      "nome": "Christopher Nolan",  
      "cidade": " Westminster, Londres",  
      "nasc": "1970-01-01T04:26:51.992Z",  
      "criadoem": "2020-05-21T04:13:55.414Z",  
      "__v": 0  
    }  
# Distribuidores<h1>    

 {  
      "_id": "5ec6b96958d6c529ecab881f",  
      "nome": "Pictures",  
      "criadoem": "2020-05-21T17:24:57.289Z",  
      "__v": 0,  
      "cidade": "Gramado"  
    },  
    {  
      "_id": "5ec94213c7adbc1ebc47d593",  
      "nome": "20th Century Fox",  
      "cidade": "São joão",  
      "criadoem": "2020-05-23T15:32:35.754Z",  
      "__v": 0  
    },  
    {  
      "_id": "5ec9422ec7adbc1ebc47d594",  
      "nome": "Americine do Brasil",  
      "cidade": "Rio de Janeiro",  
      "criadoem": "2020-05-23T15:33:02.218Z",  
      "__v": 0  
    }  

# Generos<h1>  

    {  
      "_id": "5ec6af63e1711627dcd0b842",  
      "nome": "Acao",  
      "criadoem": "2020-05-21T16:42:11.810Z",  
      "__v": 0  
    },  
    {  
      "_id": "5ec6c5b6b161730e1ca1efb4",  
      "nome": "Terror",  
      "criadoem": "2020-05-21T18:17:26.509Z",  
      "__v": 0  
    },  
    {  
      "_id": "5ec6c6ea9420f809ac2ab0a4",  
      "nome": "Suspense Policial",  
      "criadoem": "2020-05-21T18:22:34.436Z",  
      "__v": 0  
    }  

# Filmes<h1>   
   {  
      "_id": "5ec7121f02cdbe0f0094de38",  
      "nome": "Matrix",  
      "genero": {  
        "_id": "5ec6af63e1711627dcd0b842",  
        "nome": "Acao",  
        "criadoem": "2020-05-21T16:42:11.810Z",  
        "__v": 0  
      },  
      "distribuidor": {  
        "_id": "5ec6b96958d6c529ecab881f",  
        "nome": "Pictures",  
        "criadoem": "2020-05-21T17:24:57.289Z",  
        "__v": 0,  
        "cidade": "Gramado"  
      },  
      "diretor": {  
        "_id": "5ec600958887ff2314622572",  
        "nome": "Peter Jackson\t",  
        "cidade": "Pukerua Bay, Porirua",   
        "nasc": "1970-01-01T04:26:51.992Z",  
        "criadoem": "2020-05-21T04:13:55.414Z",  
        "__v": 0  
      },  
      "criadoem": "2020-05-21T23:43:27.785Z",  
      "__v": 0  
    },  
    {  
      "_id": "5ec7125202cdbe0f0094de3a",  
      "nome": "Mulan",  
      "genero": {  
        "_id": "5ec6af63e1711627dcd0b842",  
        "nome": "Acao",  
        "criadoem": "2020-05-21T16:42:11.810Z",  
        "__v": 0  
      },  
      "distribuidor": {  
        "_id": "5ec6b96958d6c529ecab881f",  
        "nome": "Pictures",  
        "criadoem": "2020-05-21T17:24:57.289Z",  
        "__v": 0,  
        "cidade": "Gramado"  
      },  
      "diretor": {   
        "_id": "5ec600958887ff2314622572",  
        "nome": "Peter Jackson\t",  
        "cidade": "Pukerua Bay, Porirua",  
        "nasc": "1970-01-01T04:26:51.992Z",  
        "criadoem": "2020-05-21T04:13:55.414Z",  
        "__v": 0  
      },  
      "sinopse": "Mulher com os olhos puxados",  
      "criadoem": "2020-05-21T23:44:18.421Z",  
      "__v": 0  
    },  
    {  
      "_id": "5ec94540554dbb1118dfb1b6",  
      "nome": "Mulher Gato",  
      "sinopse": "Mulher do Batman",  
      "criadoem": "2020-05-23T15:46:08.117Z",  
      "__v": 0  
    }  
    
# Usuarios<h1>   


{  
      "admin": true,  
      "filme": [],  
      "_id": "5ec4f4b53625b61b8ce4153e",  
      "nome": "Lucia",  
      "email": "Alterado de novdasdsasdoooo",  
      "senha": "admin",  
      "__v": 0,  
      "criadoem": "2020-05-23T16:07:42.395Z"  
    },  
    {  
      "admin": false,  
      "filme": [],  
      "_id": "5ec52dd0cc971c036054354f",  
      "nome": "Lucia",  
      "email": "Alterado de O segundo",  
      "senha": "admin",  
      "__v": 0,  
      "criadoem": "2020-05-23T16:07:42.395Z"  
    },  
    {  
      "admin": false,  
      "filme": [],  
      "_id": "5ec5f028c64cb02820fc196c",  
      "nome": "dassa",  
      "email": "Email velho",  
      "senha": "admin",  
      "criadoem": "2020-05-21T03:06:16.732Z",  
      "__v": 0  
    },  
    {  
      "admin": false,  
      "filme": [],  
      "_id": "5ec5f214600157054c90a427",  
      "nome": "dassa",  
      "email": "Email velhod",  
      "senha": "admin",  
      "criadoem": "2020-05-21T03:14:28.438Z",  
      "__v": 0  
    },  
    {  
      "admin": false,  
      "filme": [],  
      "_id": "5ec8b89bc93169200c7ce84a",  
      "nome": "Fernando",  
      "email": "Nando",  
      "senha": "admin",  
      "criadoem": "2020-05-23T05:46:03.802Z",  
      "__v": 0  
    },  
    {  
      "admin": false,  
      "filme": [],  
      "_id": "5ec92dbdf0edc10d2cf612a3",  
      "nome": "Luiz",  
      "email": "luiz",  
      "senha": "admin",  
      "criadoem": "2020-05-23T14:05:49.610Z",  
      "__v": 0  
    },  
    {  
      "admin": false,  
      "filme": [],  
      "_id": "5ec9472d554dbb1118dfb1b7",  
      "nome": "Carlos",  
      "email": "carlos",  
      "senha": "admin",  
      "criadoem": "2020-05-23T15:54:21.871Z",  
      "__v": 0  
    }  



