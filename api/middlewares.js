const jwt = require('jsonwebtoken')
const config = require('./config/autorizacao.json')
module.exports = {


    //Verifica se o Token é valido
    
    auth(req,res, next){
     const token = req.header('auth')
     console.log(token)

        if (!token) 
            return res.status(401).send({ auth: false, message: 'Acesso Negado Token não informado' }); 
        
        jwt.verify(token, config.secret, function(err, decoded) { 
            if (err) 
                return res.status(500).send({ error: err, message: 'Token inválido' }); 
            
            req.userId = decoded.id; 
            console.log("User Id: " + decoded.id)
            next(); 
        }); 
    }
 }