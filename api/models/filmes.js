const mongoose = require('../data')

//Model de Filmes
const UserSchema =  mongoose.Schema({
  nome:{
    type: String,
    unique: true,
  },
  sinopse:{
    type: String,
  },
  genero:{
   type: mongoose.Schema.Types.ObjectId,
   ref: 'generos',

  },
  distribuidor:{
   type: mongoose.Schema.Types.ObjectId,
   ref: 'distribuidores',
  },
  diretor:{
   type: mongoose.Schema.Types.ObjectId,
   ref:'diretores',
  },
  criadoem:{
    type: Date,
    default: Date.now,
  }
})
// Nomeando a Collection
const Filmes = mongoose.model('filmes', UserSchema)

module.exports = Filmes