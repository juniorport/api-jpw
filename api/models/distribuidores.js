const mongoose = require('../data')

//Model de Distribuidores
const UserSchema =  mongoose.Schema({
  nome:{
    type: String,
    unique: true,
    //require:true
  },
  cidade:{
    type: String,
   
    //require: true,

  },
  criadoem:{
    type: Date,
    default: Date.now,
  }
})
// Nomeando a Collection
const Distribuidores = mongoose.model('distribuidores', UserSchema)

module.exports = Distribuidores