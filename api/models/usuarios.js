const mongoose = require('../data')

//Model de Usuarios
const UserSchema =  mongoose.Schema({
  nome:{
    type: String,
    require: true,
  },
  email:{
    type: String,
    unique: true,
    //require: true,
    //lowercase: true,
  },
  senha:{
    type: String,
   // require:true,
   //select: false,
  },
  admin:{
    type: Boolean,
    default: false,
   // require: true
  },
  filme:[{
   type:mongoose.Schema.Types.ObjectId,
   ref: 'filmes',
   default: null,
  }],
  criadoem:{
    type: Date,
    default: Date.now,
  }
})
// Nomeando a Collection
const Usuario = mongoose.model('usuarios', UserSchema)

module.exports = Usuario