const mongoose = require('../data')

//Model de Generos
const UserSchema =  mongoose.Schema({
  nome:{
    type: String,
    unique: true,
  },
  criadoem:{
    type: Date,
    default: Date.now,
  }
})
// Nomeando a Collection
const Generos = mongoose.model('generos', UserSchema)

module.exports = Generos