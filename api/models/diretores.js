const mongoose = require('../data')

//Model de Diretores
const UserSchema =  mongoose.Schema({
  nome:{
    type: String,
     unique: true,
  },
  cidade:{
    type: String,
   //require:true,
   //select: false,
  },
  nasc:{
    type: Date,
   // require: true
  },
  criadoem:{
    type: Date,
    default: Date.now,
  }
})
// Nomeando a Collection
const Diretores = mongoose.model('diretores', UserSchema)

module.exports = Diretores

