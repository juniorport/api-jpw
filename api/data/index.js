const mongoose = require('mongoose')

//Configurando a conexao com o Mongoose

mongoose.connect("mongodb://localhost/NetFilmes",{ useNewUrlParser: true, useUnifiedTopology: true}).then(()=>{
    console.log("Mongo Conectado ...")
}).catch( (err)=>{
  console.statusCode(500).log ("Houve um erro ao conectar ao Banco: "+err)
})



module.exports = mongoose