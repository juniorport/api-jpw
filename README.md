# API COM EXPRESS+ MONGODB <h1>
# PROJETO <h2>
O proposta do projeto é a criação de um site de cadastro de filmes onde e para entrar no site o Usuario deve estar devidamente cadastrado, neste cadastro deve ter tambem as seguintes informações:  
* **Nome** - O nome do Filme  
* **Sinopse** - Uma descrição do filme  
* **Diretor** - Qual o diretor do filme    
* **Distribuidor** - Empresa responsavel pela distribuição do Filme  
* **Genero** - Qual o genero que o filme se aplica  


Este é um projeto para a materia de **Java Para WEB** e nele deve conter os seguintes requisitos para a nota maxima:  
1. Deverá conter a implementação de um CRUD para ao menos 5 recursos Ex.: Campeonato, Time, Jogador, Técnico, Partida Seção, Post, Comentário, Usuário, Tema Escola, Turma, Estudante, Professor, Disciplina Mundo, Personagem, Monstro, Local, Item
2. Cada recuso deverá conter as seguintes operações, obedecendo os principios REST: Consulta Geral ( GET ) Consulta Única ( GET ) Inserção ( POST ) Atualização ( PUT ) Deleção ( DELETE )
3. Todas as operações de consulta geral devem conter pelo menos um limitador ( ? limit=5 ) e um filtro (Ex.: ?cor_cabelo=azul
4. Deverá ser realizado tratamento de erros para todas as operações, retornando o status code dependendo do sucesso da operação (referência) 500 - Erro causado pelo servidor 400 - Erro causado pelo cliente 200 - Sucesso
5. Deverá executar todas as operações em banco de dados MongoDB. Local ou remoto.
6. O projeto deverá estar estruturado, contendo cada recurso em seu devido arquivo, separado por pastas routes/ data/ models/
7. O projeto deverá ser instalável através do comando npm install e executado através do comando npm start (ou equivalentes no yarn )
8. O projeto deverá ser disponibilizado em repositório git no GitLab ou GitHub
9. Deverá conter um README.md documentando todos os endpoints da aplicação, descrevendo o recurso. Adicionalmente, especificar também os dados utilizados
10. A API deverá incluir um middleware de autenticação, responsável por aceitar apenas clientes devidamente autenticados. O servidor deve ser capaz de gerenciar múltiplas autenticações, que deverão ser armazenadas no banco.


Neste pasta raiz do projeto esta todas contam 5 arquivos alem do README;  

1- **api** - Esta pasta onde ficara a nossa API com seus respectivos arquivos de ROUTES, MIDDLEWARES, nela tambem consta a nossa conecção com o Banco de dados e os SCHEMAS que serão utilizados;  
2- **.gitignore** - este arquivo ignora instalaçoes feitas para o desenvolvimento da API para que o projeto fique mais leve, e melhore a postagem no GitLab;  
3- **index.js** - este é o arquivo principal para a chamada de nossa API ;  
4- **package-lock.json** - Dependencias utilizadas em nossa API  
5-**package.json** - Denpendencias e informações de nossa API  

Para executar a API você deve ter instalado o MONGODB que foi nosso Banco de dados utilizado, assim como o NODE  
Links de Instalação NODE, MONGODB  
Mongo - <https://www.mongodb.com>  
Node - <https://nodejs.org/en/>  
Apos clonar o nosso projeto execute o comando  'npm install' para instalar as dependencias