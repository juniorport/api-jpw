const express = require('express')
const app = express()
const usuarios = require('./api/routes/usuarios')
const diretores = require('./api/routes/diretores')
const distribuidores = require('./api/routes/distribuidores')
const filmes = require('./api/routes/filmes')
const generos = require('./api/routes/generos')
const middlewares = require('./api/middlewares')
const cors = require('cors')


app.use(express.json())
app.use(cors())
app.use('/users', usuarios)
app.use('/diretores', diretores)
app.use('/distribuidores', distribuidores)
app.use('/filmes', filmes)
app.use('/gens', generos)


try {
    app.listen(8080, () => {
        console.log("API conectada com sucesso")
    })
} catch (error) {
    console.log(error + "Servidor não conectato")
}

